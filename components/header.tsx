import styles from "./header.module.css";
import Image from "next/image";
import { useAppSelector, useAppDispatch } from "../hooks";
import { useState } from "react";
import { type } from "os";
import Cart from "./cart";
export default function Header() {
  const mycarts = useAppSelector((state) => state.carts);
  // console.log("hello", mycarts);
  // console.log(Object.keys(mycarts).length);
  const [isCart, setIsCart] = useState(false);
  const seeCart = () => {
    isCart == true ? setIsCart(false) : setIsCart(true);
  };
  const [leftBar, setLeftBar] = useState(false);
  const displayNavbar = () => {
    leftBar == true ? setLeftBar(false) : setLeftBar(true);
  };
  return (
    <div className={styles.header}>
      <Image
        className={styles.phoneMenu}
        src="images/icon-menu.svg"
        alt="Logo"
        width={30}
        height={25}
        priority
        onClick={displayNavbar}
      />

      {/* menu mobile */}
      {leftBar == true ? (
        <div className={styles.navbarPhone}>
          <span className={styles.close} onClick={displayNavbar}>
            X
          </span>
          <li>Collections</li>
          <li>Men</li>
          <li>Women</li>
          <li>About</li>
          <li>Contact</li>
        </div>
      ) : null}
      {leftBar == false ? (
        <Image
          className={styles.logo}
          src="images/logo.svg"
          alt="Logo"
          width={100}
          height={24}
          priority
        />
      ) : null}
      <div className={styles.menu}>
        <li>Collections</li>
        <li>Men</li>
        <li>Women</li>
        <li>About</li>
        <li>Contact</li>
      </div>
      <div className={styles.right}>
        <Image
          className={styles.cart}
          src="images/icon-cart.svg"
          alt="Logo"
          width={24}
          height={24}
          priority
          onClick={seeCart}
        />
        {Object.keys(mycarts.cartState).length > 0 && (
          <span className={styles.quantity}>
            {Object.keys(mycarts.cartState).length > 0 &&
              Object.keys(mycarts.cartState).length}
          </span>
        )}
        {/* <Cart /> */}
        <Image
          src="/images/image-avatar.png"
          alt="Logo"
          width={24}
          height={24}
          priority
        />
      </div>
      {isCart === true ? <Cart /> : null}
    </div>
  );
}
