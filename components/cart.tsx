import styles from "./cart.module.css";
import Image from "next/image";
import { useAppSelector } from "../hooks";
export default function Cart() {
  const mycarts = useAppSelector((state) => state.carts);
  const data = mycarts.cartState;
  console.log("carat", data);
  return (
    <div className={styles.container}>
      <span>Cart</span>
      <div className={styles.content}>
        <ul>
          {data.length > 0 ? (
            data.map((item, idx) => (
              <>
                <li key={idx} className={styles.listCart}>
                  <Image
                    className={styles.cartImage}
                    src={`/images/${item.product.image}`}
                    alt="Logo"
                    width={50}
                    height={50}
                    priority
                  />
                  <div className={styles.cartDetail}>
                    {item.product.title}
                    <br />${item.product.price} x {item.quantity}
                    {" = "}${item.product.price * item.quantity}
                  </div>
                </li>
              </>
            ))
          ) : (
            <div className={styles.empty}>Your cart is empty.</div>
          )}
        </ul>
        {data.length > 0 && <div className={styles.checkout}>Checkout</div>}
      </div>
    </div>
  );
}
