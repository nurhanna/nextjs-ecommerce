// "use client";
import styles from "./body.module.css";
import Image from "next/image";
import { useState } from "react";
import { SET_CART } from "../reducers/cartSlice";
import { useAppSelector, useAppDispatch } from "../hooks";
import previousArrow from "../public/images/icon-previous.svg";
import nextArrow from "../public/images/icon-next.svg";
export default function Body() {
  const products = [
    {
      id: 1,
      type: "Sneaker Company",
      title: "Fall Limited Edition Sneakers",
      subtitle: ` These low-profile sneakers are your perfect casual wear companion.
        Featuring a durable rubber outer sole, they’ll withstand everything
        the weather can offer.`,
      price: 125.0,
      image: "image-product-1-thumbnail.jpg",
    },
    {
      id: 2,
      type: "Sneaker Dua",
      title: "Sneaker two",
      subtitle: ` These low-profile sneakers are your perfect casual wear companion.
          Featuring a durable rubber outer sole, they’ll withstand everything
          the weather can offer.`,
      price: 125.0,
      image: "image-product-2-thumbnail.jpg",
    },
    {
      id: 3,
      type: "Sneaker Tiga",
      title: "Sneaker Three",
      subtitle: ` These low-profile sneakers are your perfect casual wear companion.
          Featuring a durable rubber outer sole, they’ll withstand everything
          the weather can offer.`,
      price: 125.0,
      image: "image-product-3-thumbnail.jpg",
    },
    {
      id: 4,
      type: "Sneaker Empat",
      title: "Sneaker Four",
      subtitle: ` These low-profile sneakers are your perfect casual wear companion.
          Featuring a durable rubber outer sole, they’ll withstand everything
          the weather can offer.`,
      price: 125.0,
      image: "image-product-4-thumbnail.jpg",
    },
  ];
  let [quantity, setQuantity] = useState(1);
  const plus = () => {
    setQuantity(quantity + 1);
  };
  const minus = () => {
    if (quantity > 0) {
      setQuantity(quantity - 1);
    }
  };

  let [idx, setIdx] = useState(0);

  let [message, setMessage] = useState("");

  const mycarts = useAppSelector((state) => state.carts);
  const dispatch = useAppDispatch();
  const addCart = () => {
    if (quantity > 0) {
      let data = {
        quantity: quantity,
        product: products[idx],
      };
      dispatch(SET_CART(data));
    } else {
      setMessage("Quantity harus di atas 0 untuk menambahkan ke keranjang.");
      console.log("Quantity harus di atas 0 untuk menambahkan ke keranjang.");
    }
  };

  // slider
  const [slider, setSlider] = useState(0);

  return (
    <div className={styles.body}>
      <div className={styles.image}>
        <Image
          className={styles.mainImage}
          src={"/images/" + products[idx].image}
          alt="Logo"
          width={250}
          height={250}
          priority
        />
        <div className={styles.slider}>
          <div className={styles.sliderOption}>
            <Image
              src={previousArrow}
              alt="previous"
              className={styles.previous}
              onClick={() => {
                if (slider > 0) {
                  setSlider(slider - 1);
                }
              }}
            />
            <Image
              src={nextArrow}
              alt="next"
              className={styles.next}
              onClick={() => {
                if (slider < 3) {
                  setSlider(slider + 1);
                }
              }}
            />
          </div>
          <Image
            className={styles.mainSlide}
            src={"/images/" + products[slider].image}
            alt="Logo"
            width={150}
            height={150}
            priority
            onClick={() => setIdx(0)}
          />
        </div>

        <div className={styles.rowImage}>
          <Image
            className={idx == 0 ? styles.smallImageActive : styles.smallImage}
            src="/images/image-product-1-thumbnail.jpg"
            alt="Logo"
            width={50}
            height={50}
            priority
            onClick={() => setIdx(0)}
          />
          <Image
            className={idx == 1 ? styles.smallImageActive : styles.smallImage}
            src="/images/image-product-2-thumbnail.jpg"
            alt="Logo"
            width={50}
            height={50}
            priority
            onClick={() => setIdx(1)}
          />
          <Image
            className={idx == 2 ? styles.smallImageActive : styles.smallImage}
            src="/images/image-product-3-thumbnail.jpg"
            alt="Logo"
            width={50}
            height={50}
            priority
            onClick={() => setIdx(2)}
          />
          <Image
            className={idx == 3 ? styles.smallImageActive : styles.smallImage}
            src="/images/image-product-4-thumbnail.jpg"
            alt="Logo"
            width={50}
            height={50}
            priority
            onClick={() => setIdx(3)}
          />
        </div>
      </div>
      <div className={styles.detail}>
        <div className={styles.company}>SNEAKER COMPANY</div>
        <div className={styles.title}>Fall Limited Edition Sneakers</div>
        <div className={styles.subtitle}>
          These low-profile sneakers are your perfect casual wear companion.
          Featuring a durable rubber outer sole, they’ll withstand everything
          the weather can offer.
        </div>
        <div className={styles.priceContainer}>
          <div className={styles.realPrice}>$125.00</div>
          <div className={styles.discount}>50%</div>
          <div className={styles.currentPricePhone}>$250.00</div>
        </div>
        <div className={styles.currentPrice}>$250.00</div>
        <div className={styles.buttonContainer}>
          <div className={styles.calculateQuantity}>
            <Image
              className={styles.minus}
              src="images/icon-minus.svg"
              alt="Logo"
              width={30}
              height={20}
              priority
              onClick={minus}
            />
            {quantity}
            <Image
              className={styles.plus}
              src="images/icon-plus.svg"
              alt="Logo"
              width={30}
              height={30}
              priority
              onClick={plus}
            />
          </div>
          <div className={styles.add} onClick={addCart}>
            <Image
              src="images/icon-cart.svg"
              alt="Logo"
              width={24}
              height={24}
              priority
              //   onChange={}
            />
            Add to cart
          </div>
        </div>
        <div className={styles.message}> {message}</div>
      </div>
    </div>
  );
}
