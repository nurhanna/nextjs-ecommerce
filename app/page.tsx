"use client";
import styles from "./page.module.css";
import Header from "@/components/header";
import Body from "@/components/body";
import { Provider } from "react-redux";
import store from "../store";

export default function Home() {
  return (
    <Provider store={store}>
      <main className={styles.main}>
        <Header />
        <Body />
      </main>
    </Provider>
  );
}
