import { createSlice } from "@reduxjs/toolkit";
export interface CartState {
  cartState: Array<any>;
}

const initialState: CartState = {
  cartState: [
    // {
    //   quantity: 1,
    //   product: {
    //     id: 9,
    //     type: "tetst",
    //     title: "Fall Limited Edition Sneakers",
    //     subtitle: ` These low-profile sneakers are your perfect casual wear companion.
    //     Featuring a durable rubber outer sole, they’ll withstand everything
    //     the weather can offer.`,
    //     price: 125.0,
    //     image: "image-product-1-thumbnail.jpg",
    //   },
    // },
  ],
};
const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    SET_CART: (state, action) => {
      state.cartState = [...state.cartState, action.payload];
    },
  },
});

export const { SET_CART } = cartSlice.actions;
export default cartSlice.reducer;
